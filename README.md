# Reproducible builds of Devuan GNU+Linux 5.0 Daedalus

This project provides [reproducible
build](https://reproducible-builds.org/) status for
[Devuan GNU+Linux 5.0 Daedalus](https://www.devuan.org/) on
arm64.

## Status

We have reproducibly built **0%** of the
difference between **Devuan GNU+Linux 5.0 Daedalus** and
**Debian 12 Bookworm**!  That is **0%** of
the packages we have built, and we have built **0%** or
**0** of the **0** source packages to rebuild.

Devuan GNU+Linux 5.0 Daedalus (on amd64) contains binary packages
that were added/modified compared to what is in
Debian 12 Bookworm (on amd64) that corresponds to
**0** source packages.  Some binary packages exists in more
than one version, so there is a total of **0** source packages to
rebuild.  Of these we have built **0**
reproducibly out of the **0** builds so far.

We have build logs for **0** builds, which may exceed the number
of total source packages to rebuild when a particular source package
(or source package version) has been removed from the archive.  Of the
packages we built, **0** packages are reproducible and
there are **0** packages that we could not reproduce.
Building **0** package had build failures.  We do not attempt to
build **0** packages.

[[_TOC_]]

## License

This repository is updated automatically by
[debdistreproduce](https://gitlab.com/debdistutils/debdistreproduce)
but to the extent anything is copyrightable, everything is published
under the AGPLv3+ see the file [COPYING](COPYING) or
[https://www.gnu.org/licenses/agpl-3.0.en.html].

## Contact

The maintainer of this project is [Simon
Josefsson](https://blog.josefsson.org/).
